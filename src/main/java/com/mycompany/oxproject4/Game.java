/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.oxproject4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                printPlayer();
                isFinish = true;
            }
            if (table.checkDraw()) {
                printTable();
                printDraw();
                printPlayer();
                isFinish = true;
            }
            table.switchPlayer();
        }
        gameContinue();
    }

    private void printWelcome() {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
    }

    private void printTable() {
        char[][] t = table.getTable();
        System.out.println("|-----------|");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " | ");
            }
            System.out.println();
            System.out.println("|-----------|");
        }
    }

    private void printTurn() {
        System.out.println("It's player " + table.getCurrentPlayer().getSymbol() + " turn.");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row[1-3] and column[1-3] for your move [ex:1 1]:");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println("Congratulations! Player " + table.getCurrentPlayer().getSymbol() + " wins!");
    }

    private void printDraw() {
        System.out.println("The result is Draw!");
    }

    private void printPlayer() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void gameContinue() {
        Scanner kb = new Scanner(System.in);
        boolean isContinue = false;
        while (!isContinue) {
            System.out.print("Do you want to play again? (y/n) : ");
            char ans = kb.next().charAt(0);
            if (ans == 'y') {
                Game game = new Game();
                game.play();
                isContinue = true;
            } else if (ans == 'n') {
                System.out.println("Thank you for playing! Goodbye!");
                isContinue = true;
            } else {
                System.out.println("Invalid input. Please enter y or n ");
            }
        }

    }

}
